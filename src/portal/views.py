from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from portal.forms import OutOfOfficeRequestForm
from portal.models import OutOfOfficeRequest

class ProfileView(View):
    def get(self, request, *args, **kwargs):
        profile = request.user.profile
        vacation = profile.vacation_left()
        return render(request, 'profile.html', {
            'profile': profile,
            'vacation': vacation,
        })


class OutOfOfficeRequesetView(View):

    def get(self, request, *args, **kwargs):
        requests = OutOfOfficeRequest.objects.filter(requester=request.user)
        form = OutOfOfficeRequestForm(context={'request': request})
        return render(request, 'out-of-office-request.html', {
            'form': form,
            'requests': requests
        })

    def post(self, request, *args, **kwargs):

        form = OutOfOfficeRequestForm(request.POST, context={'request': request})

        if form.is_valid():
            request_object = form.save(commit=False)
            request_object.requester = request.user
            request_object.save()
        requests = OutOfOfficeRequest.objects.filter(requester=request.user)

        return render(request, 'out-of-office-request.html', {
            'form': form,
            'requests': requests
        })