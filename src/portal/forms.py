from django import forms
from portal.models import OutOfOfficeRequest
from django.conf import settings


class OutOfOfficeRequestForm(forms.ModelForm):
    message = forms.CharField(widget=forms.Textarea)
    from_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
    till_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)

    class Meta:
        model = OutOfOfficeRequest
        fields = ['from_date', 'till_date', 'message', 'request_type']

    def __init__(self, *args, **kwargs):
        self.context = kwargs.pop("context")
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(OutOfOfficeRequestForm, self).clean()
        vacation_left = self.context['request'].user.profile.vacation_left()

        from_date = cleaned_data.get("from_date")
        till_date = cleaned_data.get("till_date")

        time_delta = till_date - from_date


        return self.cleaned_data

