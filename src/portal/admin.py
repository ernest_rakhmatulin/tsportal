from django.contrib import admin
from portal import models

admin.site.register(models.Profile)
admin.site.register(models.Holiday)
admin.site.register(models.OutOfOfficeRequest)

