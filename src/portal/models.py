from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from datetime import date, datetime
from numpy import busday_count



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True, blank=True)
    image = models.ImageField(upload_to='pic_folder/')
    joined = models.DateField()
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    email = models.EmailField(null=True)
    position = models.CharField(max_length=100)
    location = models.CharField(max_length=50, default='Belarus, Minsk')


    def vacation_left_p(self):
        vacation_days_per_year_limit = 19
        vacation_pecent_left = int(self.vacation_left() / vacation_days_per_year_limit * 100)
        return vacation_pecent_left

    def vacation_left(self, ):
        start_date = self.joined
        holidays = []

        def get_years_ranges(start, end):
            delta = end.year - start.year
            years = [start.year + i for i in range(delta + 1)]
            number_of_years = len(years)
            start_dates = []
            end_dates = []
            if number_of_years == 1:
                start_dates.append(start.strftime("%Y-%m-%d"))
                end_dates.append(end.strftime("%Y-%m-%d"))
            elif number_of_years >= 2:
                for year in years:
                    start_dates.append(date(year=year, day=1, month=1))
                    end_dates.append(date(year=year, day=31, month=12))
                if start_dates[0] < start:
                    start_dates[0] = start
                if end_dates[-1] > end:
                    end_dates[-1] = end
            return start_dates, end_dates

        year_k = 0.0689

        today = datetime.now().date()
        start_dates, end_dates = get_years_ranges(start_date, today)

        earned_vacation_days = 0

        for i in range(len(start_dates)):
            busines_days_count = busday_count(
                start_dates[i].strftime("%Y-%m-%d"),
                end_dates[i].strftime("%Y-%m-%d"),
                holidays=holidays
            )
            earned_vacation_days += busines_days_count * year_k

        vacation_requests = OutOfOfficeRequest.objects.filter(
            requester=self.user,
            status=OutOfOfficeRequest.APPROVED,
            request_type=OutOfOfficeRequest.VACATION
        )

        used_days = 0

        for vacation_request in vacation_requests:
            busines_days_count = busday_count(
                vacation_request.from_date.strftime("%Y-%m-%d"),
                vacation_request.till_date.strftime("%Y-%m-%d"),
                holidays=holidays
            )
            used_days += busines_days_count

        vacation_days_left = int(round(earned_vacation_days)) - used_days
        return vacation_days_left

    def day_off_left_p(self):
        day_off_per_year_limit = 5
        day_off_pecent_left = int(self.day_off_left() / day_off_per_year_limit * 100)
        return day_off_pecent_left

    def day_off_left(self):
        day_off_per_year_limit = 5
        holidays = []

        today = datetime.now().date()
        dayoff_requests = OutOfOfficeRequest.objects.filter(
            requester=self.user,
            from_date__year=today.year,
            status=OutOfOfficeRequest.APPROVED,
            request_type=OutOfOfficeRequest.DAY_OFF
        )
        used_days = 0
        for dayoff in dayoff_requests:
            time_delta = dayoff.till_date-dayoff.from_date
            dayoff_length = time_delta.days + 1
            used_days += dayoff_length

        for vacation_request in dayoff_requests:
            busines_days_count = busday_count(
                vacation_request.from_date.strftime("%Y-%m-%d"),
                vacation_request.till_date.strftime("%Y-%m-%d"),
                holidays=holidays
            )
            used_days += busines_days_count

        day_off_per_left = day_off_per_year_limit - used_days

        return day_off_per_left


class OutOfOfficeRequest(models.Model):
    VACATION = _('Vacation')
    DAY_OFF = _('Day-off')

    REQUEST_TYPE_CHOICES = (
        ('Vacation', VACATION),
        ('Day-off', DAY_OFF),
    )
    PENDING = _('Pending')
    APPROVED = _('Approved')
    REJECTED = _('Rejected')

    STATUS_TYPE_CHOICES = (
        ('Pending', PENDING),
        ('Approved', APPROVED),
        ('Rejected', REJECTED)
    )

    requester = models.ForeignKey(User, on_delete=models.CASCADE)
    from_date = models.DateField()
    till_date = models.DateField()
    message = models.CharField(max_length=255)
    request_type = models.CharField(max_length=30, choices=REQUEST_TYPE_CHOICES)
    status = models.CharField(max_length=25, choices=STATUS_TYPE_CHOICES, default=PENDING)


class Holiday(models.Model):
    date = models.DateField()
    annual = models.BooleanField(default=False)
